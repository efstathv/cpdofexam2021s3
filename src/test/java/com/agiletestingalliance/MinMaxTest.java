package com.agiletestingalliance;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MinMaxTest {

    @Test
    public void minMaxBIsGreaterTest() throws Exception {
        assertEquals("Result", 2, new MinMax().minMax(1,2));
    }

    @Test
    public void minMaxAIsGreaterTest() throws Exception {
        assertEquals("Result", 2, new MinMax().minMax(2,1));
    }

    @Test
    public void barEmptyStringTest() throws Exception {
        assertEquals("Result", "", new MinMax().bar(""));
    }

    @Test
    public void barNullStringTest() throws Exception {
        assertNull(new MinMax().bar(null));
    }
    

    @Test
    public void barHasValueTest() throws Exception {
        assertEquals("Result", "test", new MinMax().bar("test"));
    }
}
